import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../models/Message';
import { MessageType } from '../models/MessageType';
import { SocketService } from '../services/Socket.service';

@Component({
  selector: 'app-chat-input',
  templateUrl: './chat-input.component.html',
  styleUrls: ['./chat-input.component.scss'],
})
export class ChatInputComponent implements OnInit {
  constructor(private socket: SocketService) {}

  ngOnInit(): void {}

  value: string = '';

  SendMessage() {
    this.socket.SendMessage({
      content: this.value,
      sender: 'client',
      MessageType: MessageType.Chat,
    });
  }
}
