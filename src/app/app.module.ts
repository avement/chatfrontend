import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {
  InjectableRxStompConfig,
  RxStompRPCService,
  RxStompService,
  rxStompServiceFactory,
} from '@stomp/ng2-stompjs';

import { AppComponent } from './app.component';
import { ChatInputComponent } from './chat-input/chat-input.component';
import { RxStompSocketConfig } from './services/Socket.config';
import { SocketService } from './services/Socket.service';

@NgModule({
  declarations: [AppComponent, ChatInputComponent],
  imports: [BrowserModule, FormsModule],
  providers: [
    SocketService,
    RxStompRPCService,
    {
      provide: InjectableRxStompConfig,
      useValue: RxStompSocketConfig,
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
