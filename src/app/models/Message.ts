import { MessageType } from './MessageType';

export class Message {
  content: string = '';
  sender: string = '';
  MessageType: MessageType = MessageType.Chat;
}
