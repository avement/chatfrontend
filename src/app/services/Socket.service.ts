import { Injectable } from '@angular/core';
import { InjectableRxStompConfig, RxStompService } from '@stomp/ng2-stompjs';
import * as StompJs from 'stompjs';
import { Message } from '../models/Message';

@Injectable()
export class SocketService {
  constructor(private rxStompService: RxStompService) {
    rxStompService
      .watch('/topic/public')
      .subscribe((message: StompJs.Message) => {
        console.log(message);
      });
  }

  SendMessage(data: Message) {
    console.log(JSON.stringify(data));
  }
}
